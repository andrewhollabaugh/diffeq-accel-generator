package diffeq_gen

import scala.collection.mutable.Buffer
import Chisel._
import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.rocket._
import freechips.rocketchip.tile._
import freechips.rocketchip.tilelink._
import hardfloat._

case object DiffEqSystem extends Field[ODESystem]
case object DiffEqNumericalMethod extends Field[NumericalMethod]
case object DiffEqNumMACUnits extends Field[Int]

class WithDiffEqAccel(system: ODESystem, numerical_method: NumericalMethod, num_mac_units: Int) extends Config((site, here, up) => {
  case DiffEqSystem => system
  case DiffEqNumericalMethod => numerical_method
  case DiffEqNumMACUnits => num_mac_units
  case BuildRoCC => up(BuildRoCC) ++ Seq(
    (p: Parameters) => {
      val diffeq = LazyModule.apply(new DiffEqAccel(OpcodeSet.custom2)(p))
      diffeq
    }
  )
})

class DiffEqAccel(opcodes: OpcodeSet)(implicit p: Parameters) extends LazyRoCC(opcodes) {
  override lazy val module = new DiffEqAccelImp(this, p(DiffEqSystem), p(DiffEqNumericalMethod), p(DiffEqNumMACUnits))
}

class DiffEqAccelImp(outer: DiffEqAccel, system: ODESystem, numerical_method: NumericalMethod, num_mac_units: Int) extends LazyRoCCModuleImp(outer) {
  val hfutil = HardfloatUtil

  val r_num_steps = Reg(UInt(64.W))
  val r_input_addr = Reg(UInt(64.W))
  val r_out_addr = Reg(UInt(64.W))

  val r_h = Reg(Bits(hfutil.w_total))
  val r_diff_var = Reg(Bits(hfutil.w_total))

  val rv_vars = Reg(Vec(system.vars.length, Bits(hfutil.w_total))) //vars for writing to mema
  val rv_vars_temp = Reg(Vec(system.vars.length, Bits(hfutil.w_total))) //vars being calculated in current iteration
  val rv_consts = Reg(Vec(system.consts.length, Bits(hfutil.w_total)))

  val r_mem_busy = Reg(init=false.B)
  val r_mem_index = Reg(init=UInt(0, 64.W))

  val (hw_macs: Seq[Seq[MACHW]], num_irs: Int) = GenerateMACs(system, numerical_method, num_mac_units)
  val mac_units = MACVec(num_mac_units)
  val rv_irs = Reg(Vec(num_irs, Bits(hfutil.w_total)))

  io.mem.req.valid := false.B
  io.mem.req.bits.size := log2Ceil(8).U
  io.mem.req.bits.phys := false.B
  io.mem.req.bits.signed := false.B
  io.mem.req.bits.dprv := io.cmd.bits.status.dprv
  when(io.mem.req.valid && io.mem.req.ready) {
    r_mem_busy := true.B
  }.elsewhen(io.mem.resp.valid) {
    r_mem_busy := false.B
  }

  //common states
  val s_idle = 0.U
  val s_read_req = 1.U
  val s_read_resp = 2.U
  val s_read_wait = 3.U
  val sg_first_state: Int = 4

  val r_state = Reg(init=s_idle)
  val r_p_state = Reg(init=0.U) //pipelining, within write state

  io.interrupt := false.B
  io.cmd.ready := r_state === s_idle
  io.busy := r_state =/= s_idle

  switch(r_state) {
    /* INPUTS ARGS
    * funct=0
    * rs1=step_size
    * rs2=num_steps
    *
    * funct=1 -> run
    * rs1=addr to inputs
    * [x_0, y_0, lambda, -b, -mu, c]
    *  0-0  0-1    1-0   1-1  2-0 2-1
    * rs2=addr to output
    */
    is(s_idle) {
      when(io.cmd.valid) {
        when(io.cmd.bits.inst.funct === 0.U) {
          r_h := io.cmd.bits.rs1
          r_num_steps := io.cmd.bits.rs2
        }.elsewhen(io.cmd.bits.inst.funct === 1.U) {
          r_input_addr := io.cmd.bits.rs1
          r_out_addr := io.cmd.bits.rs2
          r_state := s_read_req
        }
      }
    }
    is(s_read_req) {
      io.mem.req.valid := true.B
      io.mem.req.bits.cmd := M_XRD
      io.mem.req.bits.addr := r_input_addr
      io.mem.req.bits.data := 0.U
      when(io.mem.req.ready) {
        r_input_addr := r_input_addr + 8.U
        r_mem_index := r_mem_index + 1.U
        r_state := s_read_resp
      }
    }
    is(s_read_resp) {
      io.mem.req.valid := true.B
      when(io.mem.resp.valid) {
        val b_mem_data0: Bits = io.mem.resp.bits.data(31,0)
        val b_mem_data1: Bits = io.mem.resp.bits.data(63,32)
        var i_mem: Int = 1
        val vlen: Int = system.vars.length
        val clen: Int = system.consts.length
        val tlen: Int = vlen + clen
        var read_done: Boolean = false
        while(!read_done) {
          val i_var0 = i_mem * 2 - 2
          val i_var1 = i_var0 + 1
          when(r_mem_index === i_mem.U) {
            if(i_var1 < tlen) {
              if(i_var1 < vlen) {
                rv_vars(i_var0) := b_mem_data0
                rv_vars(i_var1) := b_mem_data1
              } else if(i_var1 == vlen) {
                rv_vars(i_var0) := b_mem_data0
                rv_consts(i_var1 - vlen) := b_mem_data1
              } else if(i_var1 > vlen) {
                rv_consts(i_var0 - vlen) := b_mem_data0
                rv_consts(i_var1 - vlen) := b_mem_data1
              }
              r_state := s_read_req
            } else {
              if(i_var1 + 1 == tlen) { //one extra var/const left (odd number)
                if(clen == 0) {
                  rv_vars(i_var0) := b_mem_data0
                } else {
                  rv_consts(i_var0 - vlen) := b_mem_data0
                }
              }
              r_mem_index := 0.U
              r_state := s_read_wait
              read_done = true
            }
          }
          i_mem += 1
        }
      }
    }
    is(s_read_wait) {
      io.mem.req.valid := false.B
      r_state := sg_first_state.U
    }
  }

  /*
   * state generator for writes and computes
   *
   * requirements for completion of state generation:
   * completed all memory writes for iteration
   * completed all compute cycles for iteration
   *
   * write cycle: contains 'n' writes, one for each var
   *
   * each loop is a clock cycle
   *
   * start with write
   * alternate between write cycle and compute until done with either
   * during each write, complete 3 compute cycles (or less if there aren't that many left)
   * if only computes remaining, do rest of computes
   * if only writes remaining, do rest of writes with inserting blank cycles inbetween
   */
  var sg_state: Int = sg_first_state
  var sg_compute_cycle_i: Int = 0
  var sg_write_var_i: Int = 0
  var sg_was_last_cycle_write: Boolean = false //used to ensure write cycles are separated by >= 1 non-write cycle
  var sg_all_states_generated: Boolean = false
  while(!sg_all_states_generated) {
    when(r_state === sg_state.U) {
      if(!sg_was_last_cycle_write && sg_write_var_i < system.vars.length) {
        sg_was_last_cycle_write = true
        io.mem.req.valid := true.B
        io.mem.req.bits.cmd := M_XWR
        io.mem.req.bits.addr := r_out_addr
        if(sg_write_var_i == system.vars.length - 1) { //only one var left to write instead of two
          io.mem.req.bits.data := Cat(Bits(0, 32.W), rv_vars(sg_write_var_i))
          sg_write_var_i += 1
        } else {
          io.mem.req.bits.data := Cat(rv_vars(sg_write_var_i + 1), rv_vars(sg_write_var_i))
          sg_write_var_i += 2
        }
        when(io.mem.resp.valid) {
          r_out_addr := r_out_addr + 8.U
          r_mem_index := r_mem_index + 1.U
          if(sg_write_var_i == system.vars.length) { //if we are on the last write cycle in the iteration, hw checks if done with all iterations
            when(r_mem_index === r_num_steps - 1.U) {
              r_mem_index := 0.U
              r_state := s_idle
            }.otherwise {
              r_state := (sg_state + 1).U
            }
          } else {
            r_state := (sg_state + 1).U
          }
        }
        for(p_state <- 0 to 3) {
          when(r_p_state === p_state.U) {
            if(sg_compute_cycle_i < hw_macs.length && p_state < 3) {
              compute_mac(sg_compute_cycle_i)
              sg_compute_cycle_i += 1
              r_p_state := (p_state + 1).U
            }
          }
        }
      } else { //compute cycles and no-ops
        sg_was_last_cycle_write = false
        io.mem.req.valid := false.B
        r_state := (sg_state + 1).U //may be overwritten later
        if(sg_compute_cycle_i >= hw_macs.length - 1) { //last compute cycle or finished computes
          if(sg_write_var_i == system.vars.length) { //done with this iteration -> done generating states
            sg_all_states_generated = true
            r_state := sg_first_state.U
            r_p_state := 0.U
          }
          //do nothing (no-op) if finished computes and not done with writes
        }
        if(sg_compute_cycle_i == hw_macs.length - 1) { //last compute cycle, copy over temp vars
          for(var_i <- 0 to system.vars.length - 1) {
            rv_vars(var_i) := rv_vars_temp(var_i)
          }
        }
        if(sg_compute_cycle_i < hw_macs.length) { //computes not finished, do the next compute, could be last compute cycle
          compute_mac(sg_compute_cycle_i)
          sg_compute_cycle_i += 1
        }
      }
    }
    sg_state += 1
  }

  def compute_mac(compute_cycle_i: Int) = {
    for((mac, mac_unit_i) <- hw_macs(compute_cycle_i).view.zipWithIndex) {
      val operands = Seq[MACHWOperand](mac.m0, mac.m1, mac.a)
      val operands_hw = Buffer[Bits]() //maybe Either[Bits, Reg]
      for(operand <- operands) {
        //assign the operand's hardware component based on operand_type
        val operand_hw = operand.operand_type match {
          case ZeroOperand => "h_0000_0000".U
          case OneOperand => "h_3f80_0000".U
          case HalfOperand => "h_3f00_0000".U
          case DeltaTOperand => r_h
          case DiffVarOperand => r_diff_var
          case VarOperand => rv_vars(operand.index)
          case ConstOperand => rv_consts(operand.index)
          case IROperand => rv_irs(operand.index)
        }
        operands_hw += operand_hw
      }
      val output_hw = mac.out.output_type match {
        case IROutput => rv_irs(mac.out.index)
        case VarOutput => if(sg_compute_cycle_i == hw_macs.length - 1) { //last compute cycle -> write directly to rv_vars
          rv_vars(mac.out.index)
        } else {
          rv_vars_temp(mac.out.index)
        }
      }
      output_hw := mac_units.execute(
        i=mac_unit_i, 
        m0=operands_hw(0), 
        m1=operands_hw(1),
        a=operands_hw(2),
        op=mac.op.U)
    }
  }
  //System.exit(0)
}

case class MACVec(num_units: Int) {
  val hfutil = HardfloatUtil

  val mac_units = for(i <- 0 until num_units) yield {
    val hf_mul_adder = Module(new MulAddRecFN(hfutil.w_exp, hfutil.w_sig))
    hf_mul_adder.io.roundingMode := consts.round_near_even
    hf_mul_adder.io.detectTininess := consts.tininess_afterRounding
    hf_mul_adder
  }
  val mac_units_io = Vec(mac_units.map(_.io))

  def execute(i: Int, m0: Bits, m1: Bits, a: Bits, op: UInt): Bits = {
    val hf_mul_adder_io = mac_units_io(i)
    mac_units_io(i).a := hfutil.fp_to_recfp(m0)
    mac_units_io(i).b := hfutil.fp_to_recfp(m1)
    mac_units_io(i).c := hfutil.fp_to_recfp(a)
    mac_units_io(i).op := op
    hfutil.recfp_to_fp(mac_units_io(i).out)(31, 0) //select lower 32 bits because recfp_to_fp outputs 33 bits
  }
}

case object HardfloatUtil {
  val w_exp = 8
  val w_sig = 24
  val w_total = (w_exp + w_sig).W
  val w_total_rec = (w_exp + w_sig + 1).W

  def fp_to_recfp(normal_in: Bits): Bits = {
    recFNFromFN(w_exp, w_sig, normal_in)
  }

  def recfp_to_fp(recoded_in: Bits): Bits = {
    fNFromRecFN(w_exp, w_sig, recoded_in)
  }

  def int_to_recfp(in: UInt): Bits = {
    val hf_int_to_recfp = Module(new INToRecFN(1, w_exp, w_sig))
    hf_int_to_recfp.io.signedIn := false.B
    hf_int_to_recfp.io.in := in
    hf_int_to_recfp.io.roundingMode := consts.round_near_even
    hf_int_to_recfp.io.detectTininess := consts.tininess_afterRounding
    hf_int_to_recfp.io.out
  }

  def recfp_to_int(in: Bits, int_width: Int): Bits = {
    val hf_recfp_to_int = Module(new RecFNToIN(w_exp, w_sig, int_width))
    hf_recfp_to_int.io.in := in
    hf_recfp_to_int.io.roundingMode := consts.round_near_even
    hf_recfp_to_int.io.signedOut := false.B
    hf_recfp_to_int.io.out
  }
}
