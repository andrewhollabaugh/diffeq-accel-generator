package diffeq_gen

import scala.collection.mutable.Buffer
import util.control._
import cc.redberry.rings
import rings.poly
import poly.multivar
import poly.PolynomialMethods._
import multivar.MonomialOrder._
import rings.scaladsl._
import util._
import syntax._

trait NumericalMethod
case object EulerMethod extends NumericalMethod
case object RK2Method extends NumericalMethod

trait MACHWOperandType
case object VarOperand extends MACHWOperandType
case object ConstOperand extends MACHWOperandType
case object DiffVarOperand extends MACHWOperandType
case object DeltaTOperand extends MACHWOperandType
case object ZeroOperand extends MACHWOperandType
case object OneOperand extends MACHWOperandType
case object HalfOperand extends MACHWOperandType
case object IROperand extends MACHWOperandType

trait MACHWOutputType
case object IROutput extends MACHWOutputType
case object VarOutput extends MACHWOutputType

case class ODESystem(eqns: Seq[ODEEquation], vars: Seq[String], consts: Seq[String], diff_var: String) {
  val vars_and_consts = vars ++ consts ++ Seq(diff_var)
}
case class ODEEquation(poly_str: String, diff_var: String)
case class MACOperand(name: String, neg: Boolean) { var done_flag: Boolean = false }
case class MAC(m0: MACOperand, m1: MACOperand, a: MACOperand, var_result: String = "", output_name: String = "")
case class MACHWOperand(operand_type: MACHWOperandType, index: Int = 0)
case class MACHWOutput(output_type: MACHWOutputType, index: Int)
case class MACHW(m0: MACHWOperand, m1: MACHWOperand, a: MACHWOperand, out: MACHWOutput, op: Int)

object GenerateMACs {
  val keyword_m0: String = "m0"
  val keyword_m1: String = "m1"
  val keyword_a: String = "a"
  val keyword_mac: String = "_mac"
  val keyword_existing_mac: String = "_exmac"
  val keyword_h: String = "_h"
  val keyword_0: String = "_0"
  val keyword_1: String = "_1"
  val keyword_half: String = "_half"

  //use rings library to factor the equation and get it in a common string output format
  def format_poly_str(poly_str: String, vars_and_consts: Seq[String]): String = {
    implicit val ring = MultivariateRing(Z, vars_and_consts.toArray)
    val poly_factored = Factor(ring(poly_str))
    ring.stringify(poly_factored)
  }

  //replace exponents in the formatted polynomial string with collections of multiplications
  def remove_exponents(poly_str_orig: String): String = {
    var poly_str = poly_str_orig
    while(poly_str.contains("^")) {
      val caret_pos = poly_str.indexOf("^")
      val v: String = poly_str.substring(caret_pos - 1, caret_pos)
      var exp: Int = poly_str.substring(caret_pos + 1, caret_pos + 2).toInt
      poly_str = poly_str.substring(0, caret_pos) ++ poly_str.substring(caret_pos + 2) //remove ^exp
      while(exp >= 2) {
        poly_str = poly_str.substring(0, caret_pos) ++ "*" ++ v ++ poly_str.substring(caret_pos)
        exp -= 1
      }
    }
    poly_str
  }
  
  case class Term(vcs: Buffer[String], neg: Boolean) //a list of variable/constants multiplied together
  case class ParenExp(terms: Buffer[Term]) //a list of terms to be added together, inside of parentheses
  case class FullExp(parens: Buffer[ParenExp]) //list of expressions in parentheses to be multiplied together

  //parse the formatted polynomial string to conver to a polynomial representation using FullExp, ParenExp, and Term
  def poly_str_to_fullexp(poly_str: String): FullExp = {
    var e = FullExp(Buffer())
    var c_prev: Char = ' '
    var cur_vc: String = "" //currently parsed variable/constant

    var find_neg_one: Boolean = false
    var factor_neg: Boolean = false //factor negative (*-1) on the next paren

    for(c <- poly_str) {
      val special_chars: Seq[Char] = Seq('(', '+', '-', '1', ')', '*')
      if(special_chars.contains(c)) {
        if(cur_vc.length > 0) {
          e.parens.last.terms.last.vcs += cur_vc
          cur_vc = ""
        }
      } else {
        cur_vc += c.toString
      }
      if(c == '(') {
        if(find_neg_one && !factor_neg) { //found -1 -> need to factor neg in next this paren
          factor_neg = true
        }
        else if(find_neg_one && factor_neg) { //neg was factored in last paren -> reset
          find_neg_one = false
          factor_neg = false
        }
        e.parens += ParenExp(Buffer())
        e.parens.last.terms += Term(Buffer(), neg=factor_neg)
      }
      else if(c == '+') {
        e.parens.last.terms += Term(Buffer(), neg=factor_neg)
      }
      else if(c == '-') {
        if(c_prev == '(') {
          e.parens.last.terms.remove(e.parens.last.terms.size - 1)
        }
        e.parens.last.terms += Term(Buffer(), neg=(!factor_neg))
      }
      else if(c == '1' && c_prev == '-') {
        e.parens.last.terms.remove(e.parens.last.terms.size - 1)
        e.parens.remove(e.parens.size - 1)
        find_neg_one = true
      }
      c_prev = c
    }
    e
  }

  def get_mac_name(macs: Seq[MAC]): String = {
    keyword_mac ++ (macs.length - 1).toString
  }

  def get_ex_mac_name(macs: Seq[MAC]): String = {
    keyword_existing_mac ++ (macs.length - 1).toString
  }

  def get_mac_term(macs: Seq[MAC]): Term = {
    Term(vcs=Buffer(get_mac_name(macs)), neg=false)
  }

  //generate linear MAC sequence from FullExp polynomial representation
  def fullexp_to_macs(e: FullExp): Seq[MAC] = {
    val macs = Buffer[MAC]()

    //find terms that only have one vc, and its previous term has only one vc
    //add new mac to add the single-vc terms
    //if more than 2 single-vc terms, keep going until there is only 1 single-vc term remaining
    for(paren <- e.parens) {
      var search_again: Boolean = true
      while(search_again) {
        search_again = false
        var prev_term_i = -1
        val loop = new Breaks
        loop.breakable {
          for((term, term_i) <- paren.terms.view.zipWithIndex) {
            if(term.vcs.size == 1) {
              if(prev_term_i != -1) { //not the first one
                val prev_term: Term = paren.terms(prev_term_i)
                macs += MAC(
                  m0=MACOperand(name=prev_term.vcs(0), neg=prev_term.neg),
                  m1=MACOperand(name=keyword_1, neg=false),
                  a=MACOperand(name=term.vcs(0), neg=term.neg))
                paren.terms(term_i) = get_mac_term(macs)
                paren.terms.remove(prev_term_i)
                search_again = true
                loop.break
              }
              prev_term_i = term_i
            }
          }
        }
      }
    }

    //find terms with more than 2 vcs
    //add new mac to multiply vcs in >=2 vc term
    //keep going until term has 2 vcs left
    for(paren <- e.parens) {
      for((term, term_i) <- paren.terms.view.zipWithIndex) {
        while(term.vcs.size > 2) {
          macs += MAC(
            MACOperand(name=term.vcs(0), neg=term.neg),
            MACOperand(name=term.vcs(1), neg=false),
            MACOperand(name=keyword_0, neg=false))
          paren.terms(term_i).vcs(0) = get_mac_name(macs)
          paren.terms(term_i).vcs.remove(1)
        }
      }
    }

    //find single-vc term (there should only be 1), new MAC to add to next term (accumulate)
    //keep doing this until only 1 term left
    for(paren <- e.parens) {
      //done if there is only one term left and it has 1 vc
      while(paren.terms.size > 1 || paren.terms(0).vcs.size > 1) {
        var single_vc_term_i: Int = -1
        val loop = new Breaks
        loop.breakable {
          for((term, term_i) <- paren.terms.view.zipWithIndex) {
            if(term.vcs.size == 1) {
              single_vc_term_i = term_i
            }
            else if(single_vc_term_i != -1) { //it found a single-vc term -> add it to the next term
              val single_vc_term: Term = paren.terms(single_vc_term_i)
              macs += MAC(
                MACOperand(name=term.vcs(0), neg=term.neg),
                MACOperand(name=term.vcs(1), neg=false),
                MACOperand(name=single_vc_term.vcs(0), neg=single_vc_term.neg))
              paren.terms(term_i) = get_mac_term(macs)
              paren.terms.remove(single_vc_term_i)
              loop.break
            }
            else { //no single-vc term -> just multiply the vcs in the current term
              macs += MAC(
                MACOperand(name=term.vcs(0), neg=term.neg),
                MACOperand(name=term.vcs(1), neg=false),
                MACOperand(name=keyword_0, neg=false))
              paren.terms(term_i) = get_mac_term(macs)
              loop.break
            }
          }
        }
      }
    }

    //multiply parens together, parens should only have a single term at this point
    while(e.parens.size > 1) {
      val term0: Term = e.parens(0).terms(0)
      val term1: Term = e.parens(1).terms(0)
      macs += MAC(
        MACOperand(name=term0.vcs(0), neg=term0.neg),
        MACOperand(name=term1.vcs(0), neg=term1.neg),
        MACOperand(name=keyword_0, neg=false))
      e.parens(0).terms(0) = get_mac_term(macs)
      e.parens.remove(1)
    }
    macs.toSeq
  }

  //concatenate MAC sequences from each equation into one big sequence
  //copies over the data for each mac individually to a new sequence
  def combine_macs(mac_seqs: Seq[Seq[MAC]]): Seq[MAC] = {
    val comb_macs: Buffer[MAC] = Buffer()
    for(macs <- mac_seqs) {
      val cur_size: Int = comb_macs.length
      for(mac <- macs) {
        val m0_new = add_to_mac_num(mac.m0.name, cur_size)
        val m1_new = add_to_mac_num(mac.m1.name, cur_size)
        val a_new = add_to_mac_num(mac.a.name, cur_size)
        comb_macs += MAC(
          MACOperand(name=m0_new, neg=mac.m0.neg),
          MACOperand(name=m1_new, neg=mac.m1.neg),
          MACOperand(name=a_new, neg=mac.a.neg),
          var_result=mac.var_result,
          output_name=keyword_mac ++ comb_macs.length.toString)
      }
    }
    comb_macs.toSeq
  }

  def add_to_mac_num(operand_name: String, add_num: Int): String = {
    var operand_new: String = operand_name
    if(operand_name.length >= keyword_mac.length &&
        operand_name.substring(0, keyword_mac.length) == keyword_mac) {
      val mac_num: Int = operand_name.substring(keyword_mac.length).toInt + add_num
      operand_new = operand_name.substring(0, keyword_mac.length) ++ mac_num.toString
    } else if(operand_new.length >= keyword_existing_mac.length &&
        operand_new.substring(0, keyword_existing_mac.length) == keyword_existing_mac) {
      operand_new = keyword_mac ++ operand_new.substring(keyword_existing_mac.length)
    }
    operand_new
  }

  def generate_macs_from_poly_str(poly_str: String, vars_and_consts: Seq[String]): Seq[MAC] = {
    println(vars_and_consts)
    println(poly_str)
    val poly_str_fmt = remove_exponents(format_poly_str(poly_str, vars_and_consts))
    println(poly_str_fmt)
    val full_exp = poly_str_to_fullexp(poly_str_fmt)
    fullexp_to_macs(full_exp)
  }

  //generate sequence of MACs for an Euler-based ODE computation
  //for each equation, executes above functions to compute polynomial, then adds additional MAC for Euler equation and combines
  def generate_macs_euler(system: ODESystem): Seq[MAC] = {
    val mac_seqs: Buffer[Seq[MAC]] = Buffer()
    for(eqn <- system.eqns) {
      val macs: Buffer[MAC] = generate_macs_from_poly_str(eqn.poly_str, system.vars_and_consts).toBuffer
      macs += MAC(
        MACOperand(name=get_mac_name(macs), neg=false),
        MACOperand(name=keyword_h, neg=false),
        MACOperand(name=eqn.diff_var, neg=false),
        var_result=eqn.diff_var)
      mac_seqs += macs.toSeq
    }
    combine_macs(mac_seqs)
  }

  def generate_macs_rk2(system: ODESystem): Seq[MAC] = {
    var macs: Seq[MAC] = Seq()
    val macs_k1_results: Buffer[String] = Buffer()
    for(eqn <- system.eqns) {
      val macs_k1: Buffer[MAC] = generate_macs_from_poly_str(eqn.poly_str, system.vars_and_consts).toBuffer
      macs_k1 += MAC(
        MACOperand(name=get_mac_name(macs_k1), neg=false),
        MACOperand(name=keyword_h, neg=false),
        MACOperand(name=keyword_0, neg=false))
      print_macs(macs)
      println("BEFORE")
      macs = combine_macs(Seq(macs, macs_k1))
      print_macs(macs)
      println("AFTER")
      macs_k1_results += get_ex_mac_name(macs)
    }
    val macs_k2_results: Buffer[String] = Buffer()
    for(eqn <- system.eqns) {
      val diff_var_substitution: String = "(" ++ system.diff_var ++ "+" ++ keyword_h ++ ")"
      var poly_str_subs = eqn.poly_str.replaceAll(system.diff_var, diff_var_substitution)
      for((eqn, macs_k1_result) <- system.eqns.zip(macs_k1_results)) {
        val k1_subs: String = "(" ++ eqn.diff_var ++ "+" ++ macs_k1_result ++ ")"
        poly_str_subs = poly_str_subs.replaceAll(eqn.diff_var, k1_subs)
      }
      val vars_and_consts: Seq[String] = system.vars_and_consts ++ Seq(keyword_h) ++ macs_k1_results
      val macs_k2: Buffer[MAC] = generate_macs_from_poly_str(poly_str_subs, vars_and_consts).toBuffer
      macs_k2 += MAC(
        MACOperand(name=get_mac_name(macs_k2), neg=false),
        MACOperand(name=keyword_h, neg=false),
        MACOperand(name=keyword_0, neg=false))
      macs = combine_macs(Seq(macs, macs_k2))
      macs_k2_results += get_ex_mac_name(macs)
    }
    for((eqn, eqn_i) <- system.eqns.view.zipWithIndex) {
      val macs_final: Seq[MAC] = Seq(
        MAC(
          MACOperand(name=macs_k1_results(eqn_i), neg=false),
          MACOperand(name=keyword_1, neg=false),
          MACOperand(name=macs_k2_results(eqn_i), neg=false)),
        MAC(
          MACOperand(name=keyword_mac ++ "0", neg=false),
          MACOperand(name=keyword_half, neg=false),
          MACOperand(name=eqn.diff_var, neg=false),
          var_result=eqn.diff_var))
      macs = combine_macs(Seq(macs, macs_final))
    }
    macs
  }

/*
 * Assign the macs to mac units in a way where the mac units can execute in parallel.
 * parallel macs is a 2d seq, where outer seq is the cycle, and inner seq is the mac to be executed by a particular mac unit in that cycle
 * Keep adding macs linearly to a cycle until it is full, then to go next cycle
 * Depedency detection: when assigning a mac, one of this mac's operands depend on the output of a mac in the current or previous cycles
 * If dependency detected, assign this mac to the next cycle, then continue assigning to current cycle
 */
  def parallelize_macs(macs: Seq[MAC], num_mac_units: Int): Seq[Seq[MAC]] = {
    var parallel_macs = Buffer[Buffer[MAC]](Buffer[MAC]())
    var mac_op_i: Int = 0
    var cur_cycle: Int = 0
    var units_assigned_to_cur_cycle: Int = 0
    while(mac_op_i < macs.length) {
      val cur_mac_op = macs(mac_op_i)
      var dependent: Boolean = false
      if(units_assigned_to_cur_cycle >= parallel_macs(cur_cycle).length) { //skip if we already put a mac here due to dependency
        var dependent_mac_cycle: Int = 0
        val loop = new Breaks
        loop.breakable {
          for(cur_cycle_to_check_i <- parallel_macs.length - 1 to cur_cycle by -1) { //iterate backwards to find most recent cycle with a dependency
            for(cur_mac_to_check <- parallel_macs(cur_cycle_to_check_i)) {
              dependent = cur_mac_to_check.output_name == cur_mac_op.m0.name || 
                          cur_mac_to_check.output_name == cur_mac_op.m1.name ||
                          cur_mac_to_check.output_name == cur_mac_op.a.name
              if(dependent) {
                dependent_mac_cycle = cur_cycle_to_check_i
                loop.break
              }
            }
          }
        }
        if(dependent) {
          var found_place_in_existing_cycles = false
          val loop = new Breaks
          loop.breakable { //check existing cycles to see if there is an available unit that is not yet assigned, otherwise add another cycle
            for(cycle_to_add_i <- dependent_mac_cycle + 1 to parallel_macs.length - 1) {
              if(parallel_macs(cycle_to_add_i).length < num_mac_units) {
                parallel_macs(cycle_to_add_i) += cur_mac_op
                found_place_in_existing_cycles = true
                loop.break
              }
            }
          }
          //println(found_place_in_existing_cycles)
          if(!found_place_in_existing_cycles) {
            parallel_macs += Buffer[MAC]()
            parallel_macs.last += cur_mac_op
          }
        } else {
          parallel_macs(cur_cycle) += cur_mac_op
        }
        mac_op_i += 1
      }
      if(!dependent) {
        units_assigned_to_cur_cycle += 1
        if(units_assigned_to_cur_cycle == num_mac_units) {
          units_assigned_to_cur_cycle = 0
          if(mac_op_i < macs.length) { //don't add stuff if this is the last macop
            cur_cycle += 1
            if(cur_cycle >= parallel_macs.length) { //only add a new cycle if we need to, there could already be one there due to dependency
              parallel_macs += Buffer[MAC]()
            }
          }
        }
      }
    }
    parallel_macs.map(_.toSeq).toSeq
  }

  //calculate the parallelization utilization of the parallel mac structure
  def calc_utilization(parallel_macs: Seq[Seq[MAC]], num_mac_units: Int): String = {
    val util_proportions = Buffer[Float]()
    for(cycle <- parallel_macs) {
      util_proportions += cycle.length.toFloat / num_mac_units
    }
    val average: Float = util_proportions.sum / util_proportions.length
    return (average * 100).toString ++ "%"
  }

  /*
   * Find where each mac output value is used last by an operand later in the computation, then set this operand's done_flag.
   * For each mac, check every all operands of macs later in the computation. If any operands match this mac's output value (dependency)
   * Finds the last mac operand with dependency, then sets the appropriate operand's flag
   * The done_flag is used by generate_reg_indexes to handle IRs
   */
  def set_ir_done_flags(parallel_macs: Seq[Seq[MAC]]): Seq[Seq[MAC]] = {
    for(cycle <- parallel_macs) {
      for(mac <- cycle) {
        var latest_cycle_with_dependency: Int = 0
        var mac_unit_with_dependency: Int = 0
        var operand_with_dependency: String = ""
        for((cycle_to_check, cycle_to_check_i) <- parallel_macs.view.zipWithIndex) {
          for((mac_to_check, mac_to_check_i) <- cycle_to_check.view.zipWithIndex) {
            var dependent: Boolean = false
            var operand_name: String = ""
            if(mac.output_name == mac_to_check.m0.name) {
              operand_name = keyword_m0
              dependent = true
            } else if(mac.output_name == mac_to_check.m1.name) {
              operand_name = keyword_m1
              dependent = true
            } else if(mac.output_name == mac_to_check.a.name) {
              operand_name = keyword_a
              dependent = true
            }
            if(dependent && cycle_to_check_i > latest_cycle_with_dependency) {
              latest_cycle_with_dependency = cycle_to_check_i
              mac_unit_with_dependency = mac_to_check_i
              operand_with_dependency = operand_name
            }
          }
        }
        val mac_to_set_flag: MAC = parallel_macs(latest_cycle_with_dependency)(mac_unit_with_dependency)
        if(operand_with_dependency == keyword_m0) {
          mac_to_set_flag.m0.done_flag = true
        } else if(operand_with_dependency == keyword_m1) {
          mac_to_set_flag.m1.done_flag = true
        } else if(operand_with_dependency == keyword_a) {
          mac_to_set_flag.a.done_flag = true
        } else if(mac.var_result == "") {
          println(mac.var_result)
          println("no dependency found for " ++ mac.output_name)
        }
      }
    }
    parallel_macs
  }

  def get_num_cycles(parallel_macs: Seq[Seq[MAC]]): Int = {
    var length_of_longest: Int = 0
    for(mac_unit_seq <- parallel_macs) {
      if(mac_unit_seq.length > length_of_longest) {
        length_of_longest = mac_unit_seq.length
      }
    }
    length_of_longest
  }

  case class IR(contents_init: String) {
    var contents: String = contents_init
    var in_use: Boolean = true
  }

  /*
   * Figures out the index of registers that will be used for each operand and output of each mac in parallel_macs
   * If operands are vars or consts, the index is derived from the operand's position in the original var/const array
   * If outputs are vars (result), similar to above
   * Uses a Seq of IR objects (case class IR) to figure out which IRs are assigned by outputs and used by operands
   * When an output goes to an IR (most of the time), it checks the in_use flag to find the first one that is not used
   *  if there are none in use, it adds a new one
   * When an operand references the output of a mac (eg "mac0") it finds the IR with the right contents
   * Indexes of the real IRs will be the same as indexes of the IR Seq used here
   */
  def generate_reg_indexes(
      parallel_macs: Seq[Seq[MAC]], 
      vars: Seq[String], 
      consts: Seq[String], 
      diff_var: String
    ): (Seq[Seq[MACHW]], Int) = {
    var hw_macs = Buffer[Buffer[MACHW]](Buffer[MACHW]()) //initialize hw_macs with first cycle
    val irs = Buffer[IR]()
    for((cycle, cycle_i) <- parallel_macs.view.zipWithIndex) {
      for(mac <- cycle) {
        var hw_output = MACHWOutput(output_type=IROutput, index=0) //placeholder
        if(mac.var_result != "") { //the output is a var/result -> find in vars array
          hw_output = MACHWOutput(
            output_type=VarOutput,
            index=vars.indexOf(mac.var_result))
        } else { //assume IR
          var reuse_ir = false
          val loop = new Breaks
          loop.breakable { //see if there is an IR that is available (not in_use)
            for((ir, ir_i) <- irs.zipWithIndex) {
              if(ir.in_use == false) {
                ir.contents = mac.output_name
                ir.in_use = true
                hw_output = MACHWOutput(
                  output_type=IROutput,
                  index=ir_i)
                reuse_ir = true
                loop.break
              }
            }
          } //no IR already there that is available -> cannot reuse, add another IR to the Seq
          if(!reuse_ir) {
            irs += IR(contents_init=mac.output_name)
            hw_output = MACHWOutput(
              output_type=IROutput,
              index=irs.length - 1)
          }
        }

        val operands = Seq[MACOperand](mac.m0, mac.m1, mac.a)
        var hw_operands = Buffer[MACHWOperand]()
        for((operand, operand_i) <- operands.zipWithIndex) {
          //if operand is in the original var/consts arrays, use the index in this array for the reg index
          if(operand.name == keyword_0) {
            hw_operands += MACHWOperand(operand_type=ZeroOperand)
          } else if(operand.name == keyword_1) {
            hw_operands += MACHWOperand(operand_type=OneOperand)
          } else if(operand.name == keyword_half) {
            hw_operands += MACHWOperand(operand_type=HalfOperand)
          } else if(operand.name == keyword_h) {
            hw_operands += MACHWOperand(operand_type=DeltaTOperand)
          } else if(operand.name == diff_var) {
            hw_operands += MACHWOperand(operand_type=DiffVarOperand)
          } else if(vars.contains(operand.name)) {
            hw_operands += MACHWOperand(
              operand_type=VarOperand,
              index=vars.indexOf(operand.name))
          } else if(consts.contains(operand.name)) {
            hw_operands += MACHWOperand(
              operand_type=ConstOperand,
              index=consts.indexOf(operand.name))
          } else { //assume IR: find the IR that has the right (string) name
            val loop = new Breaks
            loop.breakable {
              for((ir, ir_i) <- irs.zipWithIndex) {
                if(ir.contents == operand.name && ir.in_use == true) {
                  hw_operands += MACHWOperand(
                    operand_type=IROperand,
                    index=ir_i)
                  if(operand.done_flag) { //use the done flag (from prev function) to mark the IR as available
                    irs(ir_i).in_use = false
                  }
                  loop.break
                }
              }
            }
          }
        }

        //generate the op bits based on the neg flags of each operand, op bits given directly to hardfloat MulAdd
        val op = (mac.m0.neg, mac.m1.neg, mac.a.neg) match {
          case (false, false, false)  => 0
          case (false, false, true)   => 1
          case (false, true, false)   => 2
          case (false, true, true)    => 3
          case (true, false, false)   => 2
          case (true, false, true)    => 3
          case (true, true, false)    => 0
          case (true, true, true)     => 1
        }

        //add a new MACHW to the current cycle based on the previously created operands/output
        hw_macs(cycle_i) += MACHW(
          m0=hw_operands(0),
          m1=hw_operands(1),
          a=hw_operands(2),
          out=hw_output,
          op=op)
        
        //add a new cycle if we need to
        if(hw_macs(cycle_i).length == parallel_macs(cycle_i).length && cycle_i < parallel_macs.length - 1) {
          hw_macs += Buffer[MACHW]()
        }
      }
    }
    (hw_macs.map(_.toSeq).toSeq, irs.length)
  }

  def apply(system: ODESystem, numerical_method: NumericalMethod, num_mac_units: Int): (Seq[Seq[MACHW]], Int) = {
    print_system(system)
    val macs: Seq[MAC] = numerical_method match {
      case EulerMethod => generate_macs_euler(system)
      case RK2Method => generate_macs_rk2(system)
    }
    print_macs(macs, header=true)
    val parallel_macs: Seq[Seq[MAC]] = parallelize_macs(macs, num_mac_units=num_mac_units)
    print_parallel_macs(parallel_macs)
    set_ir_done_flags(parallel_macs)
    val (hw_macs: Seq[Seq[MACHW]], num_irs: Int) = generate_reg_indexes(parallel_macs, system.vars, system.consts, system.diff_var)
    print_hw_macs(hw_macs)

    println()
    println("TOTAL MAC OPS: " ++ macs.length.toString)
    println("CYCLES: " ++ parallel_macs.length.toString)
    println("MAC UNIT UTILIZATION: " ++ calc_utilization(parallel_macs, num_mac_units))
    println("TOTAL IRS: " ++ num_irs.toString)

    //System.exit(0)

    (hw_macs, num_irs)
  }

  def print_system(system: ODESystem) {
    println()
    println("SYSTEM INPUT: ")
    for(eqn <- system.eqns) {
      println(eqn)
    }
    print("vars: ")
    println(system.vars)
    print("consts: ")
    println(system.consts)
    print("diff_var: ")
    println(system.diff_var)
  }

  def print_full_exp(full_exp: FullExp) {
    println()
    println("FULLEXP: ")
    for(paren <- full_exp.parens) {
      println("PAREN:")
      for(term <- paren.terms) {
        println("TERM:")
        println(term.neg)
        for(vc <- term.vcs) {
          println(vc)
        }
        println("")
      }
    }
  }

  def print_macs(macs: Seq[MAC], header: Boolean = false) {
    if(header) {
      println()
      println("MAC SEQUENCE: ")
    }
    for((mac, mac_i) <- macs.view.zipWithIndex) {
      print(mac_i)
      print(": ")
      print(mac)
      println()
      if(mac.m0.neg) {
        print("-")
      }
      print(mac.m0.name)
      print("*")
      if(mac.m1.neg) {
        print("-")
      }
      print(mac.m1.name)
      if(mac.a.name != keyword_0) {
        if(mac.a.neg) {
          print(" - ")
        } else {
          print(" + ")
        }
        print(mac.a.name)
      }
      if(mac.var_result != "") {
        print(" -> ")
        print(mac.var_result)
      }
      println()
    }
  }

  def print_parallel_macs(parallel_macs: Seq[Seq[MAC]]) {
    println()
    println("PARALLELIZED MACS: ")
    for((cycle, cycle_i) <- parallel_macs.view.zipWithIndex) {
      print("Cycle: ")
      println(cycle_i)
      print_macs(cycle)
    }
  }

  def print_parallel_macs_old(parallel_macs: Seq[Seq[MAC]]) {
    println()
    var cur_cycle: Int = 0
    while(cur_cycle < get_num_cycles(parallel_macs)) {
      val cycle = Buffer[MAC]()
      for((mac_seq, mac_seq_i) <- parallel_macs.view.zipWithIndex) {
        if(cur_cycle < mac_seq.length) {
          cycle += mac_seq(cur_cycle)
        }
      }
      print("Cycle: ")
      println(cur_cycle)
      print_macs(cycle)
      cur_cycle += 1
    }
  }

  def print_mac_ir_flags(parallel_macs: Seq[Seq[MAC]]) {
    println()
    for((cycle, cycle_i) <- parallel_macs.view.zipWithIndex) {
      print("Cycle: ")
      println(cycle_i)
      for((mac, mac_i) <- cycle.view.zipWithIndex) {
        print(mac_i)
        print(": ")
        print(mac.output_name)
        print(": ")
        print("m0_flag=")
        print(mac.m0.done_flag)
        print(", m1_flag=")
        print(mac.m1.done_flag)
        print(", a_flag=")
        print(mac.a.done_flag)
        println()
      }
    }
  }

  def print_hw_macs(hw_macs: Seq[Seq[MACHW]]) {
    println()
    println("PARALLELIZED MACS WITH REG INDEXES: ")
    for((cycle, cycle_i) <- hw_macs.view.zipWithIndex) {
      print("Cycle: ")
      println(cycle_i)
      for((hw_mac, hw_mac_i) <- cycle.view.zipWithIndex) {
        print(hw_mac_i)
        print(": ")
        println(hw_mac)
      }
    }
  }
}
