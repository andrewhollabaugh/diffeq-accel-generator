#ifndef __DIFFEQ_H
#define __DIFFEQ_H

#define STEP_SIZE 0.1
#define NUM_STEPS 10
#define X0 10
#define Y0 10
#define COEF_LAMBDA 0.5
#define COEF_B 0.02
#define COEF_MU 0.5
#define COEF_C 0.02

#define OUT_TYPE float
//#define OUT_TYPE double
//#define OUT_TYPE unsigned short

#define FLOAT_TYPE float
//#define FLOAT_TYPE double

//#define NUM_METHOD_EULER
#define NUM_METHOD_RK2
//#define NUM_METHOD_RK4

#define PRINT_OUTPUT

FLOAT_TYPE step_size = STEP_SIZE;
unsigned long num_steps = NUM_STEPS;

FLOAT_TYPE inputs[6] = {X0, Y0, COEF_LAMBDA, COEF_B, COEF_MU, COEF_C};
OUT_TYPE output[NUM_STEPS*2] = {0};

void print_var(OUT_TYPE* var_array) {
    for(size_t i = 0; i < num_steps; i++) {
        //printf("%d\n", (int)(var_array[i]));
        //printf("%d (%x), ", (int)(var_array[i]), var_array[i]);
        printf("%d, ", (int)(var_array[i] * 1000));
    }
}

void test_output() {
    OUT_TYPE output_x[NUM_STEPS] = {0};
    OUT_TYPE output_y[NUM_STEPS] = {0};
    for(size_t i = 0; i < num_steps*2; i++) {
        if(i % 2 == 0) {
            output_x[i/2] = output[i];
        } else {
            output_y[i/2] = output[i];
        }
    }
    printf("X: ");
    print_var(output_x);
    printf("\n\n Y: ");
    //printf("\n\n");
    print_var(output_y);
    printf("\n\n");
}

#endif
