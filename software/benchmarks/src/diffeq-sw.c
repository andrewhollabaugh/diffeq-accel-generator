#include <stdio.h>
#include <stdint.h>
#include "rocc.h"
#include "diffeq.h"
#include "encoding.h"
#include "compiler.h"

#define LAMBDA_CONST inputs[2]
#define B_CONST inputs[3]
#define MU_CONST inputs[4]
#define C_CONST inputs[5]

int main() {
  unsigned long start, end;

  do {
    printf("Start basic test 1.\n");
    
    start = rdcycle();

    FLOAT_TYPE x_n = inputs[0];
    FLOAT_TYPE y_n = inputs[1];

#ifdef NUM_METHOD_EULER
    for(size_t i = 0; i < num_steps*2; i+=2) {
	output[i] = x_n;
	output[i+1] = y_n;
	FLOAT_TYPE x_np1 = x_n + (step_size * x_n * (LAMBDA_CONST - B_CONST * y_n)); //x_n+1 = x_n + dt(x_n(l + (-b)(y_n)))
	FLOAT_TYPE y_np1 = y_n + (step_size * y_n * (-MU_CONST + C_CONST * x_n)); //y_n+1 = y_n + dt(y_n(-mu)+(c)(x_n)))
	x_n = x_np1;
	y_n = y_np1;
    }
#endif
    
#ifdef NUM_METHOD_RK2
    for(size_t i = 0; i < num_steps*2; i+=2) {
        output[i] = x_n;
        output[i+1] = y_n;
        FLOAT_TYPE kx1 = step_size * x_n * (LAMBDA_CONST - B_CONST * y_n);
        FLOAT_TYPE ky1 = step_size * y_n * (-MU_CONST + C_CONST * x_n);
        FLOAT_TYPE kx2 = step_size * (x_n + kx1) * (LAMBDA_CONST - B_CONST * (y_n + ky1));
        FLOAT_TYPE ky2 = step_size * (y_n + ky1) * (-MU_CONST + C_CONST * (x_n + kx1));
        x_n += ((kx1 + kx2)/2);
        y_n += ((ky1 + ky2)/2);
    }
#endif

#ifdef NUM_METHOD_RK4
    for(size_t i = 0; i < num_steps*2; i+=2) {
	output[i] = x_n;
	output[i+1] = y_n;
	FLOAT_TYPE kx1 = step_size * x_n * (LAMBDA_CONST - B_CONST * y_n);
	FLOAT_TYPE ky1 = step_size * y_n * (-MU_CONST + C_CONST * x_n);
	FLOAT_TYPE kx2 = step_size * (x_n + kx1/2) * (LAMBDA_CONST - B_CONST * (y_n + ky1/2));
	FLOAT_TYPE ky2 = step_size * (y_n + ky1/2) * (-MU_CONST + C_CONST * (x_n + kx1/2));
	FLOAT_TYPE kx3 = step_size * (x_n + kx2/2) * (LAMBDA_CONST - B_CONST * (y_n + ky2/2));
	FLOAT_TYPE ky3 = step_size * (y_n + ky2/2) * (-MU_CONST + C_CONST * (x_n + kx2/2));
	FLOAT_TYPE kx4 = step_size * (x_n + kx3) * (LAMBDA_CONST - B_CONST * (y_n + ky3));
	FLOAT_TYPE ky4 = step_size * (y_n + ky3) * (-MU_CONST + C_CONST * (x_n + kx3));
	x_n += ((kx1 + 2*kx2 + 2*kx3 + kx4)/6);
	y_n += ((ky1 + 2*ky2 + 2*ky3 + ky4)/6);
    }
#endif

    end = rdcycle();

#ifdef PRINT_OUTPUT
    test_output();
#endif

  } while(0);

  printf("Success!\n");

  printf("Execution took %lu cycles\n", end - start);

  return 0;
}
