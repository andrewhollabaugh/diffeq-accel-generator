organization := "edu.rowan"
version := "1.0"
name := "diffeq_gen"
scalaVersion := "2.12.10"
libraryDependencies ++= Seq(
  "cc.redberry" %% "rings.scaladsl" % "2.5.5")
