# diffeq-accel-generator

A hardware accelerator generation tool to generate ODE-solving co-processors based on an input ODE system. The co-processor interfaces with a RocketChip CPU using RoCC and memory, and solves ODEs by implementing Euler/Runge-Kutta numerical method expressions in hardware using MAC operations. Written in Chisel HDL (Scala) using the Chipyard environment.

